// console.log("helooo");

// dummy database
let posts = [];
let count = 1;

// add post data
document.querySelector("#form-add-post").addEventListener('submit', (anyNameEvent) => {

	anyNameEvent.preventDefault();
	// prevents page refresh from submit

	posts.push({
		id: count,
		title: document.querySelector('#text-title').value,
		body: document.querySelector('#text-body').value
	});
	count++;

	showPosts(posts);
	alert("Successfully Added!");
});

// show postss
const showPosts = (posts) => {
	let postEntries = '';

	posts.forEach((post) => {
		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onclick="editPost('${post.id}')">Edit</button><br>
				<button onclick="deletePost('${post.id}')">Delete</button><br>
			</div>
		`;
	});

	document.querySelector('#div-post-entries').innerHTML = postEntries;
}


// edit postss
const editPost = (id) => {
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;
	
	document.querySelector("#text-edit-id").value = id;
	document.querySelector("#text-edit-title").value = title;
	document.querySelector("#text-edit-body").value = body;
}

// delete postss
const deletePost = (id) => {
	posts = posts.filter((post) => {
		if(post.id.toString() !== id){
			return post;
		}
	});

	document.querySelector(`#post-${id}`).remove();
}

document.querySelector('#form-edit-post').addEventListener('submit', (e) => {
	
	e.preventDefault();

	for(let i = 0; i < posts.length; i++) {
		// value post[i] is a number while document.querySelector('#text-edit-id').value is a string  - therefore, it is necessary to convert the number to string
		
		// hidden input #text-edit-id
		if(posts[i].id.toString() === document.querySelector('#text-edit-id').value) {
			posts[i].title = document.querySelector('#text-edit-title').value;
			posts[i].body = document.querySelector('#text-edit-body').value;

			showPosts(posts);
			alert("Successfully Updated!");

			break;
		}
	}
});